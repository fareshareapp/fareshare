package com.fareshare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.fareshare.Adapters.AddItemListAdapter;
import com.fareshare.Objects.Person;
import com.fareshare.Utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ItemAdd extends Activity {


    private List<Person> memberList = new ArrayList<Person>();
    private List<String> shareCount = new ArrayList<String>();

    AddItemListAdapter addItemListAdapter;

    String fileContents;

    private JSONObject fileContentObject;


    private Integer itemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_add);


        itemCount = getIntent().getIntExtra("itemCount", 1);
        String defaultItemName = "Item " + Integer.toString(itemCount);
        ((EditText)findViewById(R.id.itemName)).setText(defaultItemName);

        buildJsonObjectFromFile();


        addItemListAdapter = new AddItemListAdapter(this, memberList);
        ListView memberList = (ListView) findViewById(R.id.addItemList);
        memberList.setAdapter(addItemListAdapter);

        findViewById(R.id.nextItem).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int status = writeShareCountToFile();

                if (status < 0) {
                    return;
                }

                Intent intent = new Intent(getBaseContext(), ItemAdd.class);
                intent.putExtra("itemCount", itemCount + 1);
                startActivity(intent);
            }
        });

        Button finishButton = (Button) findViewById(R.id.finish);

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int status = writeShareCountToFile();

                if (status < 0) {
                    return;
                }
                Intent intent = new Intent(getBaseContext(), FinalScreen.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        Log.d("back", FileUtils.previousContent);
        try {
            FileUtils.writeToFile(FileUtils.previousContent, false);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        super.onBackPressed();
    }

    public void buildJsonObjectFromFile() {
        try {
            fileContents = FileUtils.readFromFile();
            Log.d("ItemAdd:Oncreate", "File Contents : " + fileContents);


            fileContentObject = new JSONObject(fileContents);
            JSONArray jsonArray = fileContentObject.getJSONArray("PersonList");
            for (int i=0; i<jsonArray.length(); i++ ) {
                memberList.add(new Person(jsonArray.getString(i)));
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException je) {
            je.printStackTrace();
        }
    }

    public int writeShareCountToFile() {
        String itemName = ((EditText) findViewById(R.id.itemName)).getText().toString();
        Double itemCost;
        try {
            itemCost = Double.valueOf(((EditText) findViewById(R.id.itemCost)).getText().toString());
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            Toast.makeText(ItemAdd.this, "Please enter correct amount", Toast.LENGTH_LONG).show();
            return -1;
        }

        ListView listView = (ListView) findViewById(R.id.addItemList);
        int noOfPeople = listView.getAdapter().getCount();
        JSONArray shareCountArray = new JSONArray();
        for (int i=0; i < noOfPeople; i++) {
//            Integer shareCount = Integer.valueOf(((EditText) listView.getChildAt(i).findViewById(R.id.count)).getText().toString());
            Integer shareCount = memberList.get(i).getShareCount();
            Log.d("counts", Integer.toString(shareCount));
            shareCountArray.put(shareCount);
        }

        try {
            JSONArray items;
            try {
                items = fileContentObject.getJSONArray("items");
            }catch (JSONException je) {
                items = new JSONArray();
            }

            JSONObject itemObject = new JSONObject();
            JSONObject itemSubObject = new JSONObject();
            itemSubObject.put("itemCost", itemCost);
            itemSubObject.put("shareArray", shareCountArray);
            itemObject.put(itemName, itemSubObject);

            items.put(itemObject);
            fileContentObject.put("items", items);

            FileUtils.previousContent = fileContents;
            FileUtils.writeToFile(fileContentObject.toString(), false);
            Log.d("writetoFile", fileContentObject.toString());

        } catch (JSONException je) {
            je.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return 0;
    }
















    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
