package com.fareshare;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.fareshare.Adapters.FinalPersonListAdapter;
import com.fareshare.Objects.Person;
import com.fareshare.Utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FinalScreen extends Activity {

    String fileContents;
    private JSONObject fileContentObject;
    FinalPersonListAdapter finalPersonListAdapter;

    private List<Person> memberList = new ArrayList<Person>();
    private Double allItemsCost = 0.0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_screen);

        finalPersonListAdapter = new FinalPersonListAdapter(this, memberList);
        ListView finalScreenList = (ListView) findViewById(R.id.final_screen_list);
        finalScreenList.setAdapter(finalPersonListAdapter);

        buildJsonObjectFromFile();

        TextView final_totalPrice = (TextView) findViewById(R.id.final_totalPrice);
        final_totalPrice.setText(Double.toString(Math.round(allItemsCost)));

        findViewById(R.id.final_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Home.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.final_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View billView = findViewById(R.id.billView);
                billView.setDrawingCacheEnabled(true);
                Bitmap viewBitmap = billView.getDrawingCache();
                String imageURI = FileUtils.writeImageToFile(viewBitmap, false);
                billView.setDrawingCacheEnabled(false);

                //Sheer bull shit : Intent will be reused by system if parameters are same are previous intent. Hence appending image file name with
                //system timestamp
                Intent intent = new Intent();

                /*
                This is to View the image.

                intent.setAction(Intent.ACTION_VIEW);
                Log.d("URI", imageURI);
                intent.setDataAndType(Uri.parse("file://" + imageURI), "image*//*");
                startActivity(intent);*/
                intent.setAction(Intent.ACTION_SEND);

                Log.d("imageURI", imageURI);
                intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + imageURI));
                intent.setType("image/png");
                //intent.putExtra(Intent.EXTRA_TEXT, Long.toString(System.currentTimeMillis()));
                startActivity(Intent.createChooser(intent, "Share Bill Using ..."));
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        //TODO back from final screen and then finish again adding new entries.

        Log.d("back", FileUtils.previousContent);
        try {
            FileUtils.writeToFile(FileUtils.previousContent, false);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        super.onBackPressed();
    }

/*    Used because sometimes getting black image.
        Bitmap saveView (View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        view.draw(c);
        return bitmap;
    }*/

    public void buildJsonObjectFromFile() {
        try {
            fileContents = FileUtils.readFromFile();
            Log.d("ItemAdd:Oncreate", "File Contents : " + fileContents);


            fileContentObject = new JSONObject(fileContents);
            JSONArray personList = fileContentObject.getJSONArray("PersonList");
            for (int i=0; i<personList.length(); i++ ) {
                memberList.add(new Person(personList.getString(i)));
            }

            JSONArray items = (fileContentObject.getJSONArray("items"));
            for (int i=0; i<items.length(); i++ ) {
                JSONObject newItem = items.getJSONObject(i);
                JSONObject eachItem = newItem.getJSONObject(newItem.keys().next());

                Log.d("each object", eachItem.toString());
                Double eachItemCost = eachItem.getDouble("itemCost");
                allItemsCost += eachItemCost;

                JSONArray eachItemShare = eachItem.getJSONArray("shareArray");

                int totalShare = 0;

                for (int j=0; j < memberList.size(); j++ ) {
                    int share = eachItemShare.getInt(j);
                    memberList.get(j).setShareCount(share);
                    totalShare += share;
                }

                for (int k=0; k < memberList.size();k++) {
                    Person eachPerson = memberList.get(k);
                    Double existingCost = 0.0;
                    if (eachPerson.getCost() != null) {
                        existingCost = eachPerson.getCost();
                    }
                    Double currentCost = (eachPerson.getShareCount() * eachItemCost/totalShare) ;
                    Log.d("existingCost", Double.toString(existingCost));
                    Log.d("currentCost", Double.toString(currentCost));
                    eachPerson.setCost(currentCost + existingCost);
                }
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException je) {
            je.printStackTrace();
        }
    }



























    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_final_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
