package com.fareshare.Objects;

/**
 * Created by rg on 30-Aug-15.
 */
public class Person {
    String name;
    Integer shareCount;
    Double cost;

    public Person(String personName) {
        this.name = personName;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getShareCount() {
        return shareCount;
    }

    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }
}
