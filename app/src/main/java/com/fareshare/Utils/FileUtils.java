package com.fareshare.Utils;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by rg on 29-Aug-15.
 */
public class FileUtils {
    public static String cacheDir;
    private static final String tempFileName = ".fareshare";

    public static String previousContent = "";

    public static void writeToFile(String contents, boolean append) throws IOException{
        File tempFile = new File(cacheDir, tempFileName);
        FileWriter fileWriter = new FileWriter(tempFile, append);
        try {
            fileWriter.write(contents);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
    }

    public static String readFromFile () throws IOException{
        File tempFile = new File(cacheDir, tempFileName);
        FileReader fileReader = new FileReader(tempFile);

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        StringBuilder stringBuilder = new StringBuilder();
        String currentLine;
        while ((currentLine = bufferedReader.readLine()) != null) {
            stringBuilder.append(currentLine);
        }

        return stringBuilder.toString();
    }

    public static String writeImageToFile(Bitmap bitmap, boolean append) {
        File sdCard = Environment.getExternalStorageDirectory();

        //Sheer bull shit : Intent will be reused by system if parameters are same are previous intent. Hence appending image file name with
        //system timestamp

        //TODO remove all previous images written using a regex or something because will have timestamp in the beginning.

        File tempFile = new File(sdCard.getAbsolutePath(), Long.toString(System.currentTimeMillis())+"freeshareImage.png");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(tempFile, append);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tempFile.getAbsolutePath();
    }
}
