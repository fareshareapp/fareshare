package com.fareshare;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.fareshare.Adapters.HomePersonListAdapter;
import com.fareshare.Objects.Person;
import com.fareshare.Utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Home extends Activity {

    private List<Person> memberList = new ArrayList<Person>();

    HomePersonListAdapter homePersonListAdapter;
    int personCount = 1;

    boolean trueExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        trueExit = false;

        final EditText textbox = (EditText) findViewById(R.id.personText);
        homePersonListAdapter = new HomePersonListAdapter(this, memberList);

        ListView personList = (ListView) findViewById(R.id.memberListView);
        personList.setAdapter(homePersonListAdapter);

        findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberList.add(new Person(textbox.getText().toString()));
                textbox.setText("Person " + Integer.toString(++personCount));
                homePersonListAdapter.notifyDataSetChanged();
            }
        });

        /*personList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                memberList.remove(i);
                homePersonListAdapter.notifyDataSetChanged();
            }
        });*/

        findViewById(R.id.continueHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    constructData();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                Intent intent = new Intent(Home.this, ItemAdd.class);
                intent.putExtra("itemCount", 1);
                startActivity(intent);
            }
        });

        FileUtils.cacheDir = this.getCacheDir().getAbsolutePath();
    }

    @Override
    public void onBackPressed()
    {
        if (trueExit) {
            System.exit(0);
        } else {
            Toast.makeText(this, "Press Back again to Exit", Toast.LENGTH_LONG).show();
            trueExit = true;
        }
    }



    public void removeMember(int position) {
        memberList.remove(position);
        homePersonListAdapter.notifyDataSetChanged();
    }
    public void constructData() throws IOException{
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", "FareShare");
            List<String> memberNameList = new ArrayList<String>();

            for (Person person : memberList) {
                memberNameList.add(person.getName());
            }

            JSONArray jsonArray = new JSONArray(memberNameList);
            jsonObject.put("PersonList", jsonArray);
            FileUtils.writeToFile(jsonObject.toString(), false);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }




























    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
