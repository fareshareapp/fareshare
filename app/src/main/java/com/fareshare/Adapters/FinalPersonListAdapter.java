package com.fareshare.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fareshare.FinalScreen;
import com.fareshare.Objects.Person;
import com.fareshare.R;

import java.util.List;

/**
 * Created by rg on 30-Aug-15.
 */
public class FinalPersonListAdapter extends ArrayAdapter<Person> {

    private FinalScreen finalScreen;

    public FinalPersonListAdapter(Context context, List<Person> persons) {
        super(context, 0, persons);
        finalScreen = (FinalScreen) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Person person = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_final_screen_list, parent, false);
        }


        TextView each_person = (TextView) convertView.findViewById(R.id.final_person);
        each_person.setText(person.getName());
        TextView each_person_cost = (TextView) convertView.findViewById(R.id.final_each_price);
        each_person_cost.setText(Double.toString(person.getCost()));


        return convertView;

    }
}
