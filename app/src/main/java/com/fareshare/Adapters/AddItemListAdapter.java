package com.fareshare.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fareshare.ItemAdd;
import com.fareshare.Objects.Person;
import com.fareshare.R;

import java.util.List;

/**
 * Created by rg on 27-Aug-15.
 */
public class AddItemListAdapter extends ArrayAdapter<Person> {

    private ItemAdd itemAdd;

    public AddItemListAdapter(Context context, List<Person> persons) {
        super(context, 0, persons);
        itemAdd = (ItemAdd) context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Person person = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_item_add_list, parent, false);
        }

        final ImageView count_down = (ImageView) convertView.findViewById(R.id.count_down);
        ImageView count_up = (ImageView) convertView.findViewById(R.id.count_up);

        TextView item_add_each_person = (TextView) convertView.findViewById(R.id.item_add_each_person);
        final EditText count = (EditText) convertView.findViewById(R.id.count);

/*        Bitmap coverImage = song.getCoverImage();
        if (coverImage != null) {
            imageView.setImageBitmap(coverImage);
        }*/

        item_add_each_person.setText(person.getName());

        final Integer shareCount = Integer.valueOf(count.getText().toString());

        count_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer tempCount = shareCount + 1 ;
                count.setText(Integer.toString(tempCount));
                person.setShareCount(Integer.valueOf(count.getText().toString()));
                AddItemListAdapter.this.notifyDataSetChanged();
            }
        });
        count_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shareCount > 0) {
                    Integer tempCount = shareCount - 1 ;
                    count.setText(Integer.toString(tempCount));
                }
                else {
                    count.setText("0");
                }
                person.setShareCount(Integer.valueOf(count.getText().toString()));
                AddItemListAdapter.this.notifyDataSetChanged();
            }
        });

        person.setShareCount(Integer.valueOf(count.getText().toString()));

        return convertView;

    }
}
