# FareShare : A simple bill splitting app

### Add individual participants part of the bill

![Add all participants](app/src/main/res/screenshots/add_people.jpg)

### Add items part of the bill. Add each persons share of the item. 

![Add all items](app/src/main/res/screenshots/add_item.jpg)

### Add all items part of the bill

![Add items](app/src/main/res/screenshots/add_another_item.jpg)

### Click finish to get the share of each person. Share the summary.

![Summary](app/src/main/res/screenshots/summary_share.jpg)
